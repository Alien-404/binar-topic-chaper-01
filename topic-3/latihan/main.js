// luas segitiga
const triangleArea = (base = 0, height = 0) => {
  // code
  try {
    // check wrong input
    if (typeof base !== 'number' || typeof height !== 'number') {
      throw new Error('Oops, please input an number!');
    }

    // logic
    return 0.5 * (base * height);
  } catch (error) {
    console.error(error.message);
  }
};

// volume kubus
const cubeVolume = (edge = 0) => {
  // code
  try {
    // check wrong input
    if (typeof edge !== 'number') {
      throw new Error('Oops, please input an number!');
    }

    // logic
    return edge ** 3;
  } catch (error) {
    console.error(error.message);
  }
};

// volume tabung
const tubeVolume = (radius = 0, height = 0) => {
  // code
  try {
    // check wrong input
    if (typeof radius !== 'number' || typeof height !== 'number') {
      throw new Error('Oops, please input an number!');
    }

    // logic
    return Math.PI * radius ** 2 * height;
  } catch (error) {
    console.error(error.message);
  }
};


