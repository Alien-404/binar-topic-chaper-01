// init var
let kendaraans = [];

// push data
kendaraans.push({ tipe: 'Mobil', roda: 4 });
kendaraans.push({ tipe: 'Sepeda', roda: 2 });
kendaraans.push({ tipe: 'Truk', roda: 6 });
kendaraans.push({ tipe: 'Bajaj', roda: 3 });
kendaraans.push({ tipe: 'Bus', roda: 8 });

// loop
for (let kendaraan of kendaraans) {
  console.log(`${kendaraan.tipe} memiliki ${kendaraan.roda} roda`);
}
