class Player {
  // static property
  static totalPlayer = 0;
  static isOnline = true;

  // instance property
  constructor(name, score) {
    this.name = name;
    this.score = score;

    // count player
    Player.totalPlayer++;
  }

  // instance method
  upScore(point) {
    this.score += point;
  }

  playerInfo() {
    console.log(`Player name : ${this.name} and the score is : ${this.score}`);
  }

  // static method
  static info() {
    console.log(`Current total Player : ${this.totalPlayer}`);
  }
}

// instance player
const player1 = new Player('Pesulap kuning', 2);
const player2 = new Player('Pesulap merah', 4);

// using class method
player1.upScore(45);
player1.playerInfo();

// call static
Player.info();
