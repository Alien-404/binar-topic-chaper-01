const usernames = ['admin', 'user', 'player', 'samsudin'];

const checkUsername = (username) => {
  return new Promise((resolve, reject) => {
    if (usernames.includes(username)) {
      reject('sorry, username has been used');
    } else {
      resolve('username available');
    }
  });
};

// checkUsername('user12')
//   .then((data) => console.log(data))
//   .catch((err) => console.error(err));

(async () => {
  try {
    const isValid = await checkUsername2('user');
    console.log(isValid);
  } catch (error) {
    console.log(error.message);
  }
})();
